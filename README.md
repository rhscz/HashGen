# HashGen

Simple gui hasher is program for hash checksum generator for string. It has simple Graphical User Interface to be simple as possible.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Microsoft Visual C++ Redistributable(x86) - for win32 systems - https://aka.ms/vs/17/release/vc_redist.x86.exe
Microsoft Visual C++ Redistributable(x64) - for win64 systems - https://aka.ms/vs/17/release/vc_redist.x64.exe
```

## Deployment

No install needed - just extract and use

## Built With

* [Openssl](https://www.openssl.org/docs/) - Openssl used for hashing
* [Visual Studio](https://visualstudio.microsoft.com) - Latest Visual studio used for compile

## Authors

* **Jan Hodouš(rhsCZ)** - *Initial work* - [rhsCZ](https://github.com/rhsCZ)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

